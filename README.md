# SMILE - SMartphones In LEctures

SMILE was developed at the Chair of Computer Architecture at the University of Freiburg as a student project supervised by Prof. Dr. Becker and his employees. It is made for innovative lecturers who want to use smartphones as additional tool in their lectures. In particular in large courses the interaction between students and lecturers may be enhanced. Using the quiz module the lecturers gain an overview over the degree of understanding of the lecture's content. Students, on the other hand, may give feedback at any time via the live-feedback or the Q&A module - without interrupting the flow of the lecture. Furthermore, student feedback can be collected during the term rather than after the course has concluded and therefore, can directly impact the course of the current lecture.

**The official support of the University of Freiburg has ended but we will publish the source code of SMILE here later this year.**

Contact: smile{at}informatik{dot}uni-freiburg{dot}de